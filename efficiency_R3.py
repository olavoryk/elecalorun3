import os
import ROOT
import copy
import math
from helperFunc import *
import time


start = time.time()

######################################
######## Initialization ##############
######################################


# Get files
from glob import glob
fileList = glob("/eos/home-t/thrynova/Run3L1Calo/ntuples/user.bcarlson.361106.EG.Feb18_OUTPUT")

myTree = ROOT.TChain("ntuple")
for myFile in fileList:
    myTree.AddFile(myFile)

# myCrits = [ "EM22R398" ]
myCrits = ["EM22R300","EM22R399","EM22R398","EM22R397", "EM22R395"]
myPlots = histHolder(myCrits, "Electron")

######################################
######## Event Loop
######################################

for myEvt in myTree:

    # Construct a list of truth and trigger taus
    offEles = []
    for i in range(myEvt.OffEles_pt.size()):
        # if myEvt.OffEles_flag.at(i) != 1:
        if myEvt.OffEles_isoflag.at(i)!=1 :
            continue
        tempVector = ROOT.TLorentzVector()
        tempVector.SetPtEtaPhiM(myEvt.OffEles_pt.at(i), myEvt.OffEles_eta.at(i), myEvt.OffEles_phi.at(i), 0)
        offEles += [ tempVector ]


    # Construct a list of trigger candidates for Run-3
    run3Eles = []
    for i in range(myEvt.Run3_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setDec(myEvt.Run3_L1Ele_passiso.at(i))
        myROI.setP4(myEvt.Run3_L1Ele_Et.at(i), myEvt.Run3_L1Ele_eta.at(i), myEvt.Run3_L1Ele_phi.at(i))
        myROI.setRhad(myEvt.Run3_L1Ele_rhad.at(i))
        myROI.setWstot(myEvt.Run3_L1Ele_wstot.at(i))
        myROI.setReta(myEvt.Run3_L1Ele_reta.at(i))
        run3Eles += [ myROI ]


    # Construct Turn-on Curves for true eles
    for offEle in offEles:

        # if abs(offEle.Eta()) > 2.47 or offEle.Pt() < 1000:
        if abs(offEle.Eta()) > 2.47 or offEle.Pt() < 1000 or (abs(offEle.Eta()) > 1.37 and abs(offEle.Eta()) < 1.52):
            continue

        # Find nearest candidate, Run-III
        nearestEle = None
        smallestDR = 50.0

        for candEle in run3Eles:
            myDR = offEle.DeltaR(candEle.TLV)
            if myDR < smallestDR:
                nearestEle = candEle
                smallestDR = myDR

        if smallestDR > 0.2:
            myPlots.fillTO('EM22R300', offEle.Pt(), 0)
            myPlots.fillTO('EM22R399', offEle.Pt(), 0)
            myPlots.fillTO('EM22R395', offEle.Pt(), 0)
            myPlots.fillTO('EM22R398', offEle.Pt(), 0)
            myPlots.fillTO('EM22R397', offEle.Pt(), 0)
        else:
            passV = isV_R3(nearestEle.Pt(), nearestEle.Eta(), 22)
            pass00 = R3cuts_00(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass99 = R3cuts_99(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass95 = R3cuts_95(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass98 = R3cuts_98(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass97 = R3cuts_97(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)

            myPlots.fillTO('EM22R300', offEle.Pt() ,passV and pass00 and nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R399', offEle.Pt() ,passV and pass99 and  nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R395', offEle.Pt() ,passV and pass95 and  nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R398', offEle.Pt() ,passV and pass98 and  nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R397', offEle.Pt() ,passV and pass97 and  nearestEle.Pt() > 22)

# Done

# Store output files

myNewFile = ROOT.TFile("efficiency/eff22М_00_99_98_97_95.root", "RECREATE")
myNewFile.cd()
myPlots.savePlots()
print ((time.time()- start)/60)
